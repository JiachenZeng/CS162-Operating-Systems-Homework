#include <ctype.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <termios.h>
#include <unistd.h>
#include <limits.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

#include "tokenizer.h"

/* Convenience macro to silence compiler warnings about unused function parameters. */
#define unused __attribute__((unused))

/* Whether the shell is connected to an actual terminal or not. */
bool shell_is_interactive;

/* File descriptor for the shell input */
int shell_terminal;

/* Terminal mode settings for the shell */
struct termios shell_tmodes;

/* Process group id for the shell */
pid_t shell_pgid;

/* Print the error about child program execution */
void print_error(char *error_operation) {
  printf("Error: #%d %s. (Operation: %s.)\n", errno, strerror(errno), error_operation);
}

int cmd_exit(struct tokens *tokens);
int cmd_help(struct tokens *tokens);
int cmd_pwd(struct tokens *tokens);
int cmd_cd(struct tokens *tokens);
int cmd_wait(struct tokens *tokens);

/* Built-in command functions take token array (see parse.h) and return int */
typedef int cmd_fun_t(struct tokens *tokens);

/* Built-in command struct and lookup table */
typedef struct fun_desc {
  cmd_fun_t *fun;
  char *cmd;
  char *doc;
} fun_desc_t;

fun_desc_t cmd_table[] = {
  {cmd_help, "?", "show this help menu"},
  {cmd_exit, "exit", "exit the command shell"},
  {cmd_pwd, "pwd", "print the current working directory"},
  {cmd_cd, "cd", "change the current working directory"},
  {cmd_wait, "wait", "wait until all background jobs terminated"}
};

/* Prints a helpful description for the given command */
int cmd_help(unused struct tokens *tokens) {
  for (unsigned int i = 0; i < sizeof(cmd_table) / sizeof(fun_desc_t); i++)
    printf("%s - %s\n", cmd_table[i].cmd, cmd_table[i].doc);
  return 1;
}

/* Exits this shell */
int cmd_exit(unused struct tokens *tokens) {
  exit(0);
}

/* Prints the current working directory */
int cmd_pwd(unused struct tokens *tokens) {
  char cwd[PATH_MAX];
  if (getcwd(cwd, PATH_MAX) == NULL) {
    printf("Can't get current working directory\n");
    return 0;
  }

  printf("%s\n", cwd);
  return 1;
}

/* Changes the current working directory */
int cmd_cd(struct tokens *tokens) {
  char *path = tokens_get_token(tokens, 1);
  if (chdir(path) == -1) {
    printf("No such directory\n");
    return 0;
  }
  return 1;
}

/* Wait until all child processes terminated */
int cmd_wait(struct tokens *tokens) {
  int status;
  __pid_t pid;
  while (pid = wait(&status), pid != -1) {
    printf("#%d done.\n", pid);
  }
  return 0;
}

/* Looks up the built-in command, if it exists. */
int lookup(char cmd[]) {
  for (unsigned int i = 0; i < sizeof(cmd_table) / sizeof(fun_desc_t); i++)
    if (cmd && (strcmp(cmd_table[i].cmd, cmd) == 0))
      return i;
  return -1;
}

/* Intialization procedures for this shell */
void init_shell() {
  /* Our shell is connected to standard input. */
  shell_terminal = STDIN_FILENO;

  /* Check if we are running interactively */
  shell_is_interactive = isatty(shell_terminal);

  if (shell_is_interactive) {
    /* If the shell is not currently in the foreground, we must pause the shell until it becomes a
     * foreground process. We use SIGTTIN to pause the shell. When the shell gets moved to the
     * foreground, we'll receive a SIGCONT. */
    while (tcgetpgrp(shell_terminal) != (shell_pgid = getpgrp()))
      kill(-shell_pgid, SIGTTIN);

    /* Saves the shell's process id */
    shell_pgid = getpid();

    /* Take control of the terminal */
    tcsetpgrp(shell_terminal, shell_pgid);

    /* Save the current termios to a variable, so it can be restored later. */
    tcgetattr(shell_terminal, &shell_tmodes);
  }
}

/* Resolve the filepath of FILE in the PATH */
char *resolve_path(char *file, char *path) {
  strcat(path, "/");
  strcat(path, file);
  if (!access(path, F_OK)) return path;
  else return NULL;
}

/* Find the FILE in colon seperated path string PATHS */
char *find_program(char *file, char *paths) {
  char *file_path = calloc(PATH_MAX, sizeof(char));
  char *ptr = paths;
  while (true) {
    char *next_ptr = strchr(ptr, ':');
    if (!next_ptr) next_ptr = strchr(ptr, 0);
    size_t len = next_ptr - ptr;

    strncpy(file_path, ptr, len);
    file_path[len] = 0;

    char *resolved_path = resolve_path(file, file_path);
    if (resolved_path) return resolved_path;

    if (*next_ptr) ptr = next_ptr + 1;
    else break;
  }

  free(file_path);
  return NULL;
}

/* Redirect input or output to file TOKEN according to IS_IN */
int redirect_input_output(char *token, bool is_in) {
  int opened_fd, new_fd;
  if (is_in) {
    opened_fd = open(token, O_RDWR);
    new_fd = STDIN_FILENO;
  } else {
    opened_fd = creat(token, S_IRUSR | S_IWUSR |
                             S_IRGRP | S_IWGRP |
                             S_IROTH | S_IWOTH);
    new_fd = STDOUT_FILENO;
  }

  if (opened_fd == -1) return -1;
  if (dup2(opened_fd, new_fd) == -1) return -1;

  return 0;
}

/* Break tokens into ARGVs, redirect input output accordingly */
int process_tokens(struct tokens *tokens, size_t token_len, char **argv) {
  for (size_t argv_idx = 0, tok_idx = 0; tok_idx < token_len; ) {
    char *token = tokens_get_token(tokens, tok_idx++);

    bool is_in  = !strcmp(token, "<");
    bool is_out = !strcmp(token, ">");
    if (!(is_in || is_out)) {
      if (tok_idx < token_len || strcmp(token, "&")) {
        argv[argv_idx++] = token;
      } else {
        return 1;
      }
    } else {
      token = tokens_get_token(tokens, tok_idx++);

      if (redirect_input_output(token, is_in) == -1) return -1;
    }
  }
  return 0;
}

/* Run the program provided by TOKENS */
int run_program(char *argv[]) {

  char *path = getenv("PATH");
  char *file_path = find_program(argv[0], path);
  if (!file_path) file_path = argv[0];

  int status = execv(file_path, argv);
  free(argv);
  return status;
}

int main(unused int argc, unused char *argv[]) {
  init_shell();

  static char line[4096];
  int line_num = 0;

  /* Please only print shell prompts when standard input is not a tty */
  if (shell_is_interactive)
    fprintf(stdout, "%d: ", line_num);

  while (fgets(line, 4096, stdin)) {
    /* Split our line into words. */
    struct tokens *tokens = tokenize(line);

    /* Find which built-in function to run. */
    int fundex = lookup(tokens_get_token(tokens, 0));

    if (fundex >= 0) {
      cmd_table[fundex].fun(tokens);
    } else {
      /* REPLACE this to run commands as programs. */
      int token_len = tokens_get_length(tokens);
      // if (!token_len) return 0; // No input, skip

      /* Process tokens into real arguments */
      char **argv = calloc(token_len + 1, sizeof(char*));
      int status = process_tokens(tokens, token_len, argv);
      unused bool run_foreground;
      if (status == -1) return -1;
      else run_foreground = !status;

      /* Create semaphore */
      static const char SEM_NAME[] = "/os_shell";
      sem_t *sem;
      if (run_foreground) {
        sem = sem_open(SEM_NAME, O_CREAT, 0777, 0);
        if (sem == SEM_FAILED) print_error("Parent SEM_OPEN");
      }
      
      pid_t pid = fork();
      if (0 < pid) { // Parent
        if (run_foreground) {
          /* Wait until the child setting its process group id */
          if (sem_wait(sem) == -1) print_error("SEM_WAIT");
          if (sem_close(sem) == -1) print_error("SEM_CLOSE");

          int status;
          if (!(tcsetpgrp(STDIN_FILENO, pid) == -1 && errno != ENOTTY &&
              /* The child has exited before switching foreground terminal.
                Not a perfect implementation. Errors may occurs when unexpected
                EPERM error arises and child exits just before WAITPID() */
              !(errno == EPERM && waitpid(pid, &status, WNOHANG)))) {
            waitpid(pid, &status, WUNTRACED | WCONTINUED);

            if (WIFSTOPPED(status)) printf("Process #%d stopped.\n", pid);
            if (WIFCONTINUED(status)) printf("Process #%d continued.\n", pid);

            signal(SIGTTOU, SIG_IGN);
            if (tcsetpgrp(STDIN_FILENO, getpgid(0)) == -1 && errno != ENOTTY) {
              print_error("Switch back foreground group");
            }
            signal(SIGTTOU, SIG_DFL);
          } else {
            print_error("Switch foreground group to child");
          }
        } else {
          printf("#%d runs in BG.\n", pid);
        }
      } else if (!pid) { // Child
        if (setpgid(0, 0) == -1) print_error("Child setpgid");

        /* Signals the parent to switch me to foreground terminal */
        if (run_foreground) {
          sem_t *sem = sem_open(SEM_NAME, 0);
          if (sem == SEM_FAILED) print_error("Child SEM_OPEN");
          if (sem_post(sem) == -1) print_error("SEM_POST");
          if (sem_close(sem) == -1) print_error("SEM_CLOSE");
        }

        int status = run_program(argv);
        if (status == -1) print_error("Run program");
        exit(status);
      } else { // Error
        printf("Can't fork!\n");
      }

      if (run_foreground && sem_unlink(SEM_NAME) == -1) print_error("SEM_UNLINK");
    }

    if (shell_is_interactive)
      /* Please only print shell prompts when standard input is not a tty */
      fprintf(stdout, "%d: ", ++line_num);

    /* Clean up memory */
    tokens_destroy(tokens);
  }

  return 0;
}
