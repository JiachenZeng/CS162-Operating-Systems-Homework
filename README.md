# Berkeley CS162: Operating Systems and Systems Programming Homework

I am not a Berkeley student. This homework is all done by me. I try to be correct but do not guarantee the correctness of the solutions.

# Progress

- [x] hw0
- [x] hw1 (optional left)
- [ ] hw2
- [ ] hw3
